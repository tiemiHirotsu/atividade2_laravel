<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/site.css">
</head>
<body>
    <div class="container" id="geral">
        <header>
            <h1><img class="img-fluid" src="img/logo.png"></h1>
            <nav>
                <ul>
                <li><a href="{{route ('home') }}">HOME</a></li>
                <li><a href="{{route('dicas-html')}}">HTML</a></li>
                <li><a href="{{route('dicas-javascript')}}">JAVASCRIPT</a></li>
                <li><a href="{{route('dicas-css')}}">CSS</a></li>
                <li><a href="{{('video-aulas')}}">VÍDEOS AULAS</a></li>
                <li><a href="{{route('contato')}}">CONTATO</a></li>
                </ul>
            </nav>
            <hr>
        </header>
        <section>
            @yield('conteudo')
        </section>
        <footer>
            <hr>
            <p>Todos os direitos são reservados</p>
        </footer>
    </div>
</body>
</html>