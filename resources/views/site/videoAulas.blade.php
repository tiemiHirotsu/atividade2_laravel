@extends('layout.site')

@section('titulo','Vídeo-aulas')

@section('conteudo')

    <div class="row">
        <div class="col-12">
            <h1 class="titulo-centralizado">Vídeo-aulas</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-4">
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-7">
                <h2 class="alinhadoEsquerda">Curso de HTML e CSS para iniciantes - Aula 1</h2>
                <p class="alinhadoEsquerda">Nesta primeira aula vou passar o conceito básico de HTML e mostrar como criar sua primeira página para teste. Com esta página você vai aprender a criar a estrutura mais simples de um documento HTML, definir títulos e também a codificação (charset) para seus documentos.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 videoPequeno">
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/ze9--J4PJ_4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        
            <p>GitHub, GitLab ou Bitbucket? Qual nós usamos! // CAC #6</p>
        </div>
        <div class="col-md-4 videoPequeno">
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/jyTNhT67ZyY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <p>MVC // Dicionário do Programador</p>
        </div>
        <div class="col-md-4 videoPequeno">
            <iframe width="100%" height="200" src="https://www.youtube.com/embed/GPcIjsz-2cA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <p>IDE // Dicionário do Programador</p>
        </div>
    </div>

@endsection
