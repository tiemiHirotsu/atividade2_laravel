@extends('layout.site')

@section('titulo','Home')

@section('conteudo')
    
    <div class="row">
        <div class="col">
            <img class="img-fluid imagem-media" src="img/post-3.jpg">
            <h2>Dicas de HTML</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis dolores tempora optio, consequatur suscipit exercitationem reiciendis sed id! Repudiandae minus accusantium quaerat ipsum laboriosam eveniet optio iure cumque culpa fuga?</p>
        </div>
        <div class="col">
            <img class="img-fluid imagem-media" src="img/post-4.jpg">
            <h2>O que é MVC</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis dolores tempora optio, consequatur suscipit exercitationem reiciendis sed id! Repudiandae minus accusantium quaerat ipsum laboriosam eveniet optio iure cumque culpa fuga?</p>
        </div>
        <div class="col">
            <img class="img-fluid imagem-media" src="img/post-5.jpg">
            <h2>Planejamento</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis dolores tempora optio, consequatur suscipit exercitationem reiciendis sed id! Repudiandae minus accusantium quaerat ipsum laboriosam eveniet optio iure cumque culpa fuga?</p>
        </div>
    </div>
    <div class="row">
        <class="col-12">
            <h1 class="titulo1">FAÇA DIFERENTE...</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis dolores tempora optio, consequatur suscipit exercitationem reiciendis sed id! Repudiandae minus accusantium quaerat ipsum laboriosam eveniet optio iure cumque culpa fuga?</p>
        </class>
    </div>
    
@endsection
