@extends('layout.site')

@section('titulo','Contato')

@section('conteudo')

<div class="row">
    <div class=col-12>
        <h1 class="titulo-centralizado">Contato</h1>
    </div>
</div>
<div class="row">
    <div class="col-9">
        <form action="#" method="POST">
            <div class="form-group">
                <input type="text" name="nome" value="Nome..." class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="telefone" value="Telefone..." class="form-control">
            </div>
            <div class="form-group">
                <input type="text" name="email" value="E-mail..." class="form-control">
            </div>
            <div class="form-group">
                <textarea placeholder="Mensagem?" class="form-control" rows="5"></textarea>
            </div>
        </form>
        <button type="button" class="btn btn-primary">Enviar</button>
    </div>
    <div class="col-3">
        <h3>Marília</h3>
        Av Sampaio Vidal 587<br>
        Phone: +14 1292 0991<br>
        E-mail: <span class="email1">support@mysite.com</span>
        <br><br>
        <h3>São Paulo</h3>
        Av Paulista 201<br>
        Telefone: +11 1478 2587<br>
        E-mail: <span class="email1">usa@mysite.com</span>
    </div>
</div>


@endsection