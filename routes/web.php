<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('/dicas-html', function () {
    return view('site.dicasHtml');
})->name('dicas-html');

Route::get('/dicas-javascript', function () {
    return view('site.dicasJavaScript');
})->name('dicas-javascript');

Route::get('/dicas-css', function () {
    return view('site.dicasCss');
})->name('dicas-css');

Route::get('/video-aulas', function () {
    return view('site.videoAulas');
})->name('video-aulas');

Route::get('/contato', function () {
    return view('site.contato');
})->name('contato');
